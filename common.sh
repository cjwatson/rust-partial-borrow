# -*- shell-script -*-

# Copyright 2021 Ian Jackson and contributors
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

: ${CARGO:=cargo}

# see argument.rs, re -Zmiri-permissive-provenance
MIRIFLAGS+=' -Zmiri-permissive-provenance'
export MIRIFLAGS

x () { echo "= $*"; "$@"; }

run_cargo () {
    x ${CARGO} "$@"
}

run_cargo_lf () {
    case "$CARGO" in
    *nailing-cargo*)
	run_cargo --linkfarm=git --clean-linkfarm "$@" ;;
    *)
	run_cargo "$@" ;;
    esac
}
