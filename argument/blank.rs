#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[repr(C)]
pub struct Partial<Pa> {
  a: Module::F_a<Pa, String, Struct>,
}

impl PartialBorrow for Struct {
  type All_No = Partial<No>;
  type All_Const = Partial<Const>;
  type All_Mut = Partial<Mut>;
  type Fields = Module::Fields;
  const FIELDS: Self::Fields = Module::FIELDS;
}

#[allow(non_camel_case_types)]
impl<P, T> Deref for Module::F_a<P, T, Struct>
where
  P: IsRef,
{
  type Target = T;

  fn deref(&self) -> &T
  where
    T: Sized,
  {
    unsafe {
      let p: *const Self = self;
      let p: *const Struct = p as usize as _;
      let offset = offset_of!(Struct, a);
      let p: *const u8 = p as _;
      let p = p.add(offset);
      let p: *const T = p as _;
      let p: &T = p.as_ref().unwrap();
      p
    }
  }
}

#[allow(non_camel_case_types)]
impl<P, T> DerefMut for Module::F_a<P, T, Struct>
where
  P: IsMut,
{
  fn deref_mut(&mut self) -> &mut T
  where
    T: Sized,
  {
    unsafe {
      let p: *mut Self = self;
      let p: *mut Struct = p as usize as _;
      let offset = offset_of!(Struct, a);
      let p: *mut u8 = p as _;
      let p = p.add(offset);
      let p: *mut T = p as _;
      let p: &mut T = p.as_mut().unwrap();
      p
    }
  }
}

#[allow(non_camel_case_types)]
impl<P, T> Debug for Module::F_a<P, T, Struct>
where
  T: Debug,
  P: IsRefOrNot,
{
  fn fmt<'r>(&'r self, f: &mut Formatter) -> fmt::Result {
    if let Some(Const) = <P as IsRefOrNot>::REF {
      let downgraded = unsafe {
        transmute::<
          &'r Module::F_a<P, T, Struct>,
          &'r Module::F_a<Const, T, Struct>,
        >(self)
      };
      Debug::fmt(&**downgraded, f)
    } else {
      Formatter::write_str(f, "_")
    }
  }
}

#[allow(non_camel_case_types)]
impl<Pa, N> Adjust<N, 0usize> for Partial<Pa> {
  type Adjusted = Partial<N>;
}

#[allow(non_camel_case_types)]
impl<Ra, Pa> AsRef<Partial<Ra>> for Partial<Pa>
where
  Ra: IsDowngradeFrom<Pa>,
{
  fn as_ref(&self) -> &Partial<Ra> {
    Downgrade::downgrade(self)
  }
}

#[allow(non_camel_case_types)]
impl<Ra, Pa> AsMut<Partial<Ra>> for Partial<Pa>
where
  Ra: IsDowngradeFrom<Pa>,
{
  fn as_mut(&mut self) -> &mut Partial<Ra> {
    Downgrade::downgrade_mut(self)
  }
}

#[allow(non_camel_case_types)]
impl<Ra, Pa> Downgrade<Partial<Ra>> for Partial<Pa>
where
  Ra: IsDowngradeFrom<Pa>,
{
  fn downgrade(input: &Self) -> &Partial<Ra> {
    unsafe {
      let input = input as *const _;
      (input as *const Partial<Ra>).as_ref().unwrap()
    }
  }

  fn downgrade_mut(input: &mut Self) -> &mut Partial<Ra> {
    unsafe {
      let input = input as *mut _;
      (input as *mut Partial<Ra>).as_mut().unwrap()
    }
  }
}

#[allow(non_camel_case_types)]
impl<Ra, Pa> SplitOff<Partial<Ra>> for Partial<Pa>
where
  Ra: IsDowngradeFrom<Pa>,
{
  type Remaining = Partial<<Ra as IsDowngradeFrom<Pa>>::Remaining>;

  fn split_off<'r>(
    input: &'r Partial<Pa>,
  ) -> (&'r Partial<Ra>, &'r Self::Remaining) {
    unsafe {
      let input = input as *const _;
      (
        (input as *const Partial<Ra>).as_ref().unwrap(),
        (input as *const Self::Remaining).as_ref().unwrap(),
      )
    }
  }

  fn split_off_mut<'r>(
    input: &'r mut Partial<Pa>,
  ) -> (&'r mut Partial<Ra>, &'r mut Self::Remaining) {
    unsafe {
      let input = input as *mut _;
      (
        (input as *mut Partial<Ra>).as_mut().unwrap(),
        (input as *mut Self::Remaining).as_mut().unwrap(),
      )
    }
  }
}

#[allow(non_camel_case_types)]
impl<'r, Ra, Sa, Pa> From<&'r Partial<Pa>>
  for (&'r Partial<Ra>, &'r Partial<Sa>)
where
  Pa: CanSplitInto<Ra, Sa>,
{
  fn from(input: &'r Partial<Pa>) -> Self {
    SplitInto::split_into(input)
  }
}

#[allow(non_camel_case_types)]
impl<'r, Ra, Sa, Pa> From<&'r mut Partial<Pa>>
  for (&'r mut Partial<Ra>, &'r mut Partial<Sa>)
where
  Pa: CanSplitInto<Ra, Sa>,
{
  fn from(input: &'r mut Partial<Pa>) -> Self {
    SplitInto::split_into_mut(input)
  }
}

#[allow(non_camel_case_types)]
impl<Ra, Sa, Pa> SplitInto<Partial<Ra>, Partial<Sa>> for Partial<Pa>
where
  Pa: CanSplitInto<Ra, Sa>,
{
  fn split_into<'r>(
    input: &'r Partial<Pa>,
  ) -> (&'r Partial<Ra>, &'r Partial<Sa>) {
    unsafe {
      let input = input as *const _;
      (
        (input as *const Partial<Ra>).as_ref().unwrap(),
        (input as *const Partial<Sa>).as_ref().unwrap(),
      )
    }
  }

  fn split_into_mut<'r>(
    input: &'r mut Partial<Pa>,
  ) -> (&'r mut Partial<Ra>, &'r mut Partial<Sa>) {
    unsafe {
      let input = input as *mut _;
      (
        (input as *mut Partial<Ra>).as_mut().unwrap(),
        (input as *mut Partial<Sa>).as_mut().unwrap(),
      )
    }
  }
}

#[allow(non_camel_case_types)]
impl<Ra> AsRef<Partial<Ra>> for Struct
where
  Ra: IsDowngradeFrom<Mut>,
{
  fn as_ref(&self) -> &Partial<Ra> {
    Downgrade::downgrade(self)
  }
}

#[allow(non_camel_case_types)]
impl<Ra> AsMut<Partial<Ra>> for Struct
where
  Ra: IsDowngradeFrom<Mut>,
{
  fn as_mut(&mut self) -> &mut Partial<Ra> {
    Downgrade::downgrade_mut(self)
  }
}

#[allow(non_camel_case_types)]
impl<Ra> Downgrade<Partial<Ra>> for Struct
where
  Ra: IsDowngradeFrom<Mut>,
{
  fn downgrade(input: &Self) -> &Partial<Ra> {
    unsafe {
      let input = input as *const _ as usize;
      (input as *const Partial<Ra>).as_ref().unwrap()
    }
  }

  fn downgrade_mut(input: &mut Self) -> &mut Partial<Ra> {
    unsafe {
      let input = input as *mut _ as usize;
      (input as *mut Partial<Ra>).as_mut().unwrap()
    }
  }
}

#[allow(non_camel_case_types)]
impl<Ra> SplitOff<Partial<Ra>> for Struct
where
  Ra: IsDowngradeFrom<Mut>,
{
  type Remaining = Partial<<Ra as IsDowngradeFrom<Mut>>::Remaining>;

  fn split_off<'r>(
    input: &'r Struct,
  ) -> (&'r Partial<Ra>, &'r Self::Remaining) {
    unsafe {
      let input = input as *const _ as usize;
      (
        (input as *const Partial<Ra>).as_ref().unwrap(),
        (input as *const Self::Remaining).as_ref().unwrap(),
      )
    }
  }

  fn split_off_mut<'r>(
    input: &'r mut Struct,
  ) -> (&'r mut Partial<Ra>, &'r mut Self::Remaining) {
    unsafe {
      let input = input as *mut _ as usize;
      (
        (input as *mut Partial<Ra>).as_mut().unwrap(),
        (input as *mut Self::Remaining).as_mut().unwrap(),
      )
    }
  }
}

#[allow(non_camel_case_types)]
impl<'r, Ra, Sa> From<&'r Struct> for (&'r Partial<Ra>, &'r Partial<Sa>)
where
  Mut: CanSplitInto<Ra, Sa>,
{
  fn from(input: &'r Struct) -> Self {
    SplitInto::split_into(input)
  }
}

#[allow(non_camel_case_types)]
impl<'r, Ra, Sa> From<&'r mut Struct>
  for (&'r mut Partial<Ra>, &'r mut Partial<Sa>)
where
  Mut: CanSplitInto<Ra, Sa>,
{
  fn from(input: &'r mut Struct) -> Self {
    SplitInto::split_into_mut(input)
  }
}

#[allow(non_camel_case_types)]
impl<Ra, Sa> SplitInto<Partial<Ra>, Partial<Sa>> for Struct
where
  Mut: CanSplitInto<Ra, Sa>,
{
  fn split_into<'r>(input: &'r Struct) -> (&'r Partial<Ra>, &'r Partial<Sa>) {
    unsafe {
      let input = input as *const _ as usize;
      (
        (input as *const Partial<Ra>).as_ref().unwrap(),
        (input as *const Partial<Sa>).as_ref().unwrap(),
      )
    }
  }

  fn split_into_mut<'r>(
    input: &'r mut Struct,
  ) -> (&'r mut Partial<Ra>, &'r mut Partial<Sa>) {
    unsafe {
      let input = input as *mut _ as usize;
      (
        (input as *mut Partial<Ra>).as_mut().unwrap(),
        (input as *mut Partial<Sa>).as_mut().unwrap(),
      )
    }
  }
}

#[allow(non_camel_case_types)]
impl<Pa> Deref for Partial<Pa>
where
  Pa: IsRef,
{
  type Target = Struct;

  fn deref(&self) -> &Self::Target {
    unsafe {
      let p: *const Self = self as _;
      let p: *const Self::Target = p as usize as _;
      let p = p.as_ref().unwrap();
      p
    }
  }
}

#[allow(non_camel_case_types)]
impl<Pa> Debug for Partial<Pa>
where
  Module::F_a<Pa, String, Struct>: Debug,
{
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    let mut fields = Formatter::debug_struct(f, "Partial");
    fmt::DebugStruct::field(&mut fields, "a", &self.a);
    fields.finish()
  }
}

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
pub mod Module {
  use super::*;

  #[repr(C)]
  pub struct F_a<P, T, S> {
    p: P,
    t: PhantomData<T>,
    s: PhantomData<*const S>,
  }
  pub struct Fields {
    pub a: usize,
  }
  pub const FIELDS: Fields = Fields { a: 0usize };
}
