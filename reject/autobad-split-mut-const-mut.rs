// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use partial_borrow::prelude::*;

#[derive(PartialBorrow,Default)]
struct X { a: usize }

fn main(){
  let mut x = X::default();
  let i: &mut partial!(X mut a) = x.as_mut();
  let (e,f): (&mut partial!(X const a),
              &mut partial!(X mut a)) = i.into();
}
