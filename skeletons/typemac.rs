// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

// This is a skeleton demonstrating a simple open-coded version
// of the traits and types.

#![allow(dead_code)]

mod partial {

  #[derive(Debug,Clone,Copy)] pub struct Not;
  #[derive(Debug,Clone,Copy)] pub struct Ref;
  #[derive(Debug)]            pub struct Mut;

  pub trait AdjPerm<NP, const F: usize> {
    type R;
  }

  #[allow(non_camel_case_types)]
  pub trait Pb {
    type Def_Not;
    type Def_Ref;
    type Def_Mut;
    type Fields;
    const FIELDS: Self::Fields;
  }
}

#[allow(dead_code)] // for here only
struct AB<'c,DT> where DT: XT {
  a: usize,
  c: &'c str,
  d: DT,
}

trait XT { }
impl XT for () { }

#[allow(dead_code)] #[allow(non_camel_case_types)]
/* as for AB */ struct AB__parts<'c,PA,PC,PD,DT> where DT: XT {
  a: AB__::F_a<PA, usize,   AB<'c,DT>>,
  c: AB__::F_c<PC, &'c str, AB<'c,DT>>,
  d: AB__::F_d<PD, DT,      AB<'c,DT>>,
}
impl<'c,DT> partial::Pb for AB<'c,DT> where DT: XT {
  type Def_Not = AB__parts<'c, partial::Not, partial::Not, partial::Not, DT>;
  type Def_Ref = AB__parts<'c, partial::Ref, partial::Ref, partial::Ref, DT>;
  type Def_Mut = AB__parts<'c, partial::Mut, partial::Mut, partial::Mut, DT>;
  type Fields = AB__::Fields;
  const FIELDS: AB__::Fields = AB__::FIELDS;
}

pub trait G {
  type Mut;
}

  impl<'c,Fa,Fb,Fd,DT,NP> partial::AdjPerm<NP, 0> for AB__parts<'c,Fa,Fb,Fd,DT>
    where AB<'c,DT>: partial::Pb, DT: XT
    { type R = AB__parts<'c, Fa, NP, Fd, DT>;
  }

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
#[allow(unused_imports)]
pub /* as for AB */ mod AB__ {
  use std::marker::PhantomData;
  use super::partial::{self, Not, Ref, Mut, AdjPerm};

  pub struct F_a<P,T,S>
  {
    #[allow(dead_code)] p: P,
    t: PhantomData<T>,
    s: PhantomData<*const S>,
  }
  pub struct F_c<P,T,S> {
    #[allow(dead_code)] p: P,
    t: PhantomData<T>,
    s: PhantomData<*const S>,
  }
  pub struct F_d<P,T,S> {
    #[allow(dead_code)] p: P,
    t: PhantomData<T>,
    s: PhantomData<*const S>,
  }

  pub struct Fields {
    pub a: usize
  }
  pub const FIELDS: Fields = Fields {
    a: 0,
  };

/*

  pub trait Field {
    type Not_a;
    type Ref_a;
    type Mut_a;

    type Not_b;
    type Ref_b;
    type Mut_b;
  }
  
  impl<Fa,Fb> Field for super::AB__parts<Fa,Fb> {
    type Not_a = super::AB__parts<Not, Fb>;
    type Ref_a = super::AB__parts<Ref, Fb>;
    type Mut_a = super::AB__parts<Mut, Fb>;

    type Not_b = super::AB__parts<Fa, Not>;
    type Ref_b = super::AB__parts<Fa, Ref>;
    type Mut_b = super::AB__parts<Fa, Mut>;
  }*/
  
//  trait XXField<const F: usize> {
//    type Fld_b;
  //  }
//  pub struct KK<const F: usize>;

/*  impl XXField for XField {
    type Fld_b = ();
  }*/
}
//pub use AB__::Field;

//type Next = Start::Fld_b;

//const K: usize = AB::__field_a();

type Start = <AB<'static,()> as partial::Pb>::Def_Not;
type Next = <Start as partial::AdjPerm<
                              partial::Mut,
  { <
      AB<'static,()> as partial::Pb
      >::FIELDS.a
  }
                             >
             >::R;

#[allow(dead_code)]
pub fn wat(_p: // &mut parts!(AB mut b)
//                 &mut <<AB as Pb>::Def_Not as AB__::Field>::Mut_b
()
) {
//  let _: &mut AB__parts<partial::Not, partial::Mut> = p;
}
