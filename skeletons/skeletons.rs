// Copyright 2021 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

mod typemac;
mod byhand;

fn main() {
  byhand::check();
}
